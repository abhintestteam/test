<!-- import api/1.0/repositories/abhintestteam/test/src/6f9d5f334d20bdb37d92f46c7ab42dcb2f212b12/import.md -->

# Changes to this guide

This file is intended to serve as a checklist/guideline. It is expected that
people will follow these guidelines in making their pull requests and that
reviewers will use this as an aid during the review process.

## Get buy-in from your team

These guidelines don't mean much if a lot of people disagree with them. So
if you're making a change here, make sure to add people knowledgeable in this
area as reviewers, even if it means having to get more than 2 people to
approve that pull-request.

## Good guidelines are specific and measurable

It is really hard to follow vague guidelines. For example "Write pretty
Python code" is a very vague guideline and could easily spark disagreement
between the author and the reviewers of the pull request. On the other
hand, "Write PEP8 compliant Python code" is a much better guideline. It
specifies exactly what is required and how to test whether the guideline
is being followed.

# Who should be reviewing a pull request

## Security

If you're making changes to the authentication backends, OAuth token management,
scopes or anything that may seem like it could open us or our customers to security
risk you should include a member of the security team on the PR.

https://extranet.atlassian.com/display/SECURITY/Security+Home#SecFrontPage-MeettheTeam

## Database

If you're making changes to database queries, adding database objects or think you might
be significantly impacting the database in any way, include a DBA on the PR.

## QA

Make sure your project's QA partner is CCd on your pull request so they are aware that it is in flight.

# Testing

## Write tests for features and bugfixes

Tests should be included along with bugfixes and feature work.
Including them with the application helps ensure the code is working as intended
while guarding against future breakage.

If you need help deciding what should be tested, reach out to your QA engineer!

If you don't think you should have tests, you should include a brief explanation
about why tests are not appropriate for your pull request.

# Database migrations

## Access models in your migration via `apps.get_model` instead of importing

For example, use `apps.get_model("bb", "Repository")` instead of
`from bitbucket.apps.bb.models import Repository`.

The reason is that `apps.get_model` will give you access to frozen models
that will contain the model as it was when you wrote this migration. For
instance, if migration `0005` deleted a column, the migrations before `0005`
will need to operate on a model that had that column. If those migrations
just imported the model, they would probably break after `0005` was introduced
because the imported model wouldn't have the column that `0005` dropped.

## Watch out for `Access Exclusive` locks!

Quote from the
[postgres 9.2 docs](http://www.postgresql.org/docs/9.2/static/explicit-locking.html):

> ACCESS EXCLUSIVE
>
>    Conflicts with locks of all modes (ACCESS SHARE, ROW SHARE, ROW
     EXCLUSIVE, SHARE UPDATE EXCLUSIVE, SHARE, SHARE ROW EXCLUSIVE,
     EXCLUSIVE, and ACCESS EXCLUSIVE). This mode guarantees that the holder
     is the only transaction accessing the table in any way.
>
>    Acquired by the ALTER TABLE, DROP TABLE, TRUNCATE, REINDEX, CLUSTER,
     and VACUUM FULL commands. This is also the default lock mode for LOCK
     TABLE statements that do not specify a mode explicitly.

When any DB operation attempts to acquire an `Access Exclusive` lock,

1. all other processes that are queued up to acquire **any** lock on that
table need to release their locks
2. all other processes that attempt to acquire **any** lock on that table
after the DB operation in question has been queued for locking need to be
queued for locking behind it. These processes would normally have probably
acquired their locks almost immediately had an `Access Excusive` lock not
been in the picture.
3. the operation in question has to acquire its lock, do the work and release
the lock.

The result of such an `Access Exclusive` lock on a busy table is catastrophic
and can bring down the site.

Make sure to run the `migration_lock_explain` script on the migrations you
create and check for any `Access Exclusive` locks on any busy tables like
`bb_repository`, `bb_userprofile`, `auth_user` etc.

```
(bitbucket)➜  bitbucket git:(tkells/mlx_ignore_toast) ✗ ./manage.py migration_lock_explain projects 0007 --with_kind

AccessExclusiveLock:
    projects_project (Table)
    projects_project_3c6e0b8a (Index)
    projects_project_5e7b1936 (Index)
    projects_project_8c0ff365 (Index)
    projects_project_key_79c3d75eab7e763d_like (Index)
    projects_project_owner_id_24f67955ee50d547_uniq (Index)
    projects_project_pkey (Index)
    projects_project_uuid_1162818a45a1ee90_uniq (Index)
ShareLock:
    projects_project (Table)
```

For example, in the session above, an `Access Exclusive` lock was acquired
on `projects_project`, which at the time this migration was run, was not a
particularly busy table.

# APIs

This topic contains guidelines for maintaining and writing new API (both
public and private) endpoints. While not religiously enforced, we aim for
endpoints to be as REST-ful as possible.

## Linking and Discoverability

We aim to follow [HATEOAS](http://en.wikipedia.org/wiki/HATEOAS) principals to
ensure all API endpoints link to one another such that, given an initial
resource URL, a client that has no prior knowledge of our APIs, can discover
all endpoints by way of following the links contained in response documents.

This reduces developers reliance on documentation, while giving us more
flexibility. Clients are encouraged to minimize hardcoding of API URLs and to
use the URLs returned in API responses.

Every object that is uniquely addressable will contain a links element that
contains a link to itself and possibly links to other representations of
itself, or further information. An example is the links element in a commit
object:

```
"links": {
  "self": {
      "href": "https://api.bitbucket.org/1.0/repositories/evzijst/bitbucket"
  },
  "html": {
    "href": "https://api.bitbucket.org/evzijst/bitbucket"
  },
  "clone": [
    {
      "href": "https://api.bitbucket.org/evzijst/bitbucket.git",
      "name": "https"
    },
    {
      "href": "ssh://git@bitbucket.org/erik/bitbucket.git",
      "name": "ssh"
    }
  ]
}
```

Links are objects that at least contain a rel and href element, but can
contain other elements too.

We somewhat follow [HAL](http://stateless.co/hal_specification.html) here.
A link key is its rel attribute and it contains a mandatory href element. On
top of that there are optional elements, including name and title that can be
used to disambiguate links that share the same ref key. In this case, the
links are contained in a list.

Links can support [URI Templates](http://tools.ietf.org/html/rfc6570), in
which case they will carry the `templated=true` element.

## URL Naming

Each resource type should have a collections URL. Hitting that will return a
paginated list (e.g. `/2.0/snippets`).

This URL may support sorting through URL parameters. Query-string based
filtering may also be supported (e.g. `/2.0/repositories?role=contributor`).

A resource's `self` link should be located underneath the collections URL
(e.g. `/2.0/snippets/c9y9`) and share that same plural base as the collection.

For URL segments that are composed of multiple words, we concatenate the words
together instead of delimiting them with hyphens(`-`) or underscores(`_`) or
using `camelCase` notation. For example, notice the "pullrequests" in
`https://bitbucket.org/api/2.0/repositories/bitbucket/bitcounter/pullrequests`.

## Naming fields and their values in the document

Fields in the document should use "underscore" notation. The reason is that
some languages (like JavaScript) allow objects to be accessed directly,
without using strings (`myObject.things_and_stuff`). A hyphen-delimited string
would not work in that case. Understandably, it would be nice to use
camelCase in this scenario, but we already have underscore notation in our
APIs and JS based applications are not the only consumers in town.

## Document type metadata

In a lot of our APIs, we return polymorphic collections (collections of
different types of objects). For example, the "refs" API can return Hg
bookmarks, Hg branches and Git branches in the same collection. For such
purposes, we return a `type` field in ALL our documents, so the consumer of
our API wouldn't have to infer the type of a document from the fields.

The value of the `type` field for any object should be underscore separated.
The reason is that consumers of the API might want to map classes to object
types in our API by looking up the `type` field in our objects, in which case,
we should allow for the possibility of such maps to be symbol (and not just
string) based.

It should be noted that the presence of a `type` attribute for a resource
document does not guarantee the presence of all the fields that represent the
resource. That is, we are allowed to have a compressed representation of a
resource embedded inside another resource without created a separate `type`
for the condensed version.

## Response Content-Types

Our APIs primarily support JSON, but endpoint implementations should never
prevent us from supporting other formats (XML, YAML, ASN.1). Serialization to
JSON is done outside the endpoints and any endpoint that produces its own JSON
strings instead of serializable objects will be unable to support other
formats. As a consequence, endpoint implementations should never make
assumptions on the response encoding.

## Request Content-Types

Most endpoints that support POSTs or PUTs currently accept
`application/x-www-url-form-encoded` requests (aka regular form posts). This
makes it easy to script against the APIs using things like curl, but has the
drawback that it is restricted to flat data structures and everything is
always a string.

Endpoints may also support complex request body encoding, such as JSON, but
deserialization must be left up to the framework (currently Piston). Piston
has rich support for various formats for transparently encoding and decoding
both request and response bodies through its pluggable emitters.

Using request.data instead of `request.POST` to access the deserialized
request body decouples the endpoint from the data format. However, the lowest
common denominator are the flat, string-based key/value pairs of a form post
and so relying on complex data structures in request bodies will make it
impossible to support simple form posts and therefore harder for clients to
integrate as a simple:

```
$ curl -X POST https://api.bitbucket.org/... -d foo=bar
```

turns into:

```
$ curl -X POST https://api.bitbucket.org/... \
    -H "ContentType: application/json" -d '{"foo": "bar", ...}'
```

Note that Piston supports Django form validation through its
`piston.utils.validate` decorator which is compatible with its pluggable
emitters.

## Error Handling

When an endpoint fails, it is the endpoint's responsibility to return the
appropriate status code, which requires anticipating error conditions and
handling them accordingly. Uncaught exceptions always turn into a `500`
response.

This means that explicit input validation is required. If an endpoint takes an
integer value as input, but fails to explicitly verify that the value is
indeed numerical, a `ValueError` will kill the request and lead to a `500`,
where it should have served a `400`. Again, Django's form validation can be
helpful and eliminate a lot of boilerplate code.

Error documents should not contain HTML and respect the client's `Accept`
header value.

An error response looks like this:

```
{
  "error":
  {
    "message": "Pull request creation failed",
    "detail": "An optional, human-readable description of the error. This may include things like a stacktrace.",
    "id": "f4f5fae1001de22d566547712d29425e1d4df2da",
    "fields": {
      "source": ["Source branch missing"]
    },
  }
}
```

* `message`: a short, one-line description of the error (required)
* `detail`: long, multi-line, human-readable description of the error
  (optional)
* `id`: a Sentry error code (or similar identifier that would help
  troubleshoot on our end) (optional)
* `fields`: useful for reporting problems on a form post with multiple fields
  where problems with each field can be reported independently (optional)

## Pagination

Endpoints that return collections of objects should always apply pagination.
Paginated collections are always wrapped in the following wrapper object:

```
{
  "size": 5421,
  "page": 2,
  "pagelen: 10,
  "next": "https://api.bitbucket.org/2.0/repositories/pypy/pypy/commits?page=3",
  "previous": "https://api.bitbucket.org/2.0/repositories/pypy/pypy/commits?page=1",
  "values": [
    ...
  ]
}
```

Pagination is often page-bound, with a query parameter page indicating which
page is to be returned.

However, clients are not expected to construct URLs themselves by manipulating
the page number query parameter. Instead, the response contains a link to the
next page. This link should be treated as an opaque location that is not to be
constructed by clients or even assumed to be predictable. The only contract
around the next link is that it will return the next chunk of results.

Lack of a next link in the response indicates the end of the collection.

* `size`: the total number of elements in the response (optional)
* `page`: the current page that is returned (optional)
* `pagelen`: the current page size, on some endpoints this may be configurable
  by the client by passing along a different value using the `pagelen=n` query
  parameter (optional)
* `next`: the link to the next page (optional – missing only on the last page
  of a collection)
* `previous`: the link the the previous page (optional – endpoints are not
  required to provide backward navigation)
* `values`: a list of up to `pagelen` objects (required)

Pagination can be applied to endpoints using the paginated decorator.

## Cachability

All endpoints should aim to be as cachable as possible. This covers a number
of things:

* Make an effort to provide meaningful `ETag` values in each response to
  facilitate
  [conditional GETs](http://tools.ietf.org/html/draft-ietf-httpbis-p4-conditional-22).
  Make sure ETag generation is very fast. Django's
  [etag decorator](https://docs.djangoproject.com/en/dev/topics/conditional-view-processing/)
  is helpful.
* Make an effort to find out when a resource was last modified and return
  that as the `Last-Modified` response header. Django's `last_modified`
  decorator is helpful.
* Consider applying other HTTP cache headers to optimize cachability, e.g.
  `Cache-Control`, `Expires`, `Max-Age`, etc.
* Aim for URLs to be stable.

The last one implies that if a URL always returns the same response, it
becomes highly cachable. For instance,

> https://api.bitbucket.org/2.0/repositories/pypy/pypy/patch/f4f5fae1001de22d566547712d29425e1d4df2da

always returns the exact same, immutable raw patch document, which is fairly
expensive for us to generate, especially if the diff is large. And if it is
large, it can also take up a lot bandwidth and latency on the network.
Luckily, its immutability makes this response a very good candidate for
caching.

While it is not difficult to return the commit's own timestamp as the
`Last-Modified` header, it's even cheaper to just echo its `SHA1` as the
`ETag`. `ETag` and `Last-Modified` serve the same purpose and the server is
free to choose either one and so in this case we'd only return an `ETag`
header.

We can also safely send a very high `Expires` value.

Unfortunately, the following URL cannot be cached, as it uses a branch name
instead of an immutable `SHA1`:

>https://api.bitbucket.org/2.0/repositories/pypy/pypy/patch/master

To avoid expensive patch generation every time this URL is hit, we can instead
resolve the reference to the target `SHA1` and then send a `302` redirect to
the URL with the branch name replaced with the `SHA1`, which applies all the
right caching headers. It's advisable to apply this to abbreviated `SHA1`s
as well.

Finally an endpoint could decide to apply active, application-level caching
inside its own implementation. However,
[One cache equals one bug](https://extranet.atlassian.com/display/~tom/2008/09/14/One+cache+equals+one+bug)
, so only do this after all of the above.

## Response Object Serialization

Object serialization is implemented through the
`bitbucket.apps.api.decorators.serialized` decorator and pluggable mapper
system that maps Python classes to serializers.

Service methods can return any complex data structure as long as there are
mappers configured for all objects that make up the response.

Objects are to be serialized in a predictable manner, with a representation
consistent across all API endpoints, meaning that there is only one
representation for a user object, regardless of which endpoint it returns.

Many objects have foreign keys on other objects and whenever an object is
included as a member of another object, its representation must be identical
to the object's `self` document, except that it typically contains only a
subset of its fields. Situations where endpoints return information on a user,
repo or any other object type in a representation inconsistent with the
object's `self` layout, possibly because a real `User` object is not easily
available at runtime, must be avoided.

As a general rule, objects included as a foreign key should be reduced to the
fewest possible fields while still being usable. This means finding a balance
between richness of the API and performance: including more data means fewer
round trips, but also makes responses a lot bigger and more expensive to
generate. Once a minimum selection of fields have been selected, it is
recommended to use this configuration in every endpoint that returns the
object. The links element is required to always be present.

When a foreign key relationship is not or no longer backed by a database
record (e.g. an audit log record for an action on a repository that has since
been deleted, or a commit author that couldn't be resolved to a Bitbucket user
account), the response can return an alternative representation, alongside the
canonical representation which can be omitted, similar to how commit authors
are encoded:

```
"author":
{
  "raw": "Brian Nguyen <bnguyen@atlassian.com>",
  "user":
  {
    "username": "bnguyen",
    "display_name": "Brian Nguyen",
    "links": [
      {
        "href": "https://api.bitbucket.org/1.0/users/bnguyen",
        "rel": "self"
      },
      {
        "href": "https://bitbucket.org/bnguyen",
        "rel": "html"
      }
    ]
  }
}
```

Here the `user` element is present only when the commit's author string could
successfully be resolved to a user account.

## Date and Times

Datetime objects are returned as `ISO-8601` strings that may include timezone
information.

Generally, timestamps will be in UTC with an explicit offset of "00:00",
however it's acceptable for non-UTC strings to be returned as long as they
include the UTC-offset where appropriate.

## Security

The API is accessible under multiple locations:

* its own dedicated domain: `https://api.bitbucket.org`
* as part of the site itself: `https://bitbucket.org/!api/`

The domain it is accessed under has implications for the available
authentication methods, which in turn has an impact on CRSF protection
enforcement.

`api.bitbucket.org` can be accessed using Basic HTTP Authentication,
OAuth 1.0/2.0, Connect JWT and API Keys. However, since we never issue
session cookies for this domain, it is not vulnerable to CSRF attacks and
as a result POSTs, PUTs and DELETEs never require the use of a CSRF token.

However, since the API is also heavily used by the site itself, relying on
session cookie authentication, the API is also available under
`https://bitbucket.org/!api/` which as a consequence enforces strict CSRF
protection through the use of (Django) CSRF middleware tokens that are
required for all mutating HTTP methods.

Note that `https://bitbucket.org/!api` is an internal part of Bitbucket
and should never be used by any external application.

## Public Vs Private

All endpoints under the 2.0 version are considered to be "official" public
APIs and must maintain backwards compatibility and be properly documented.
They should follow the guidelines set out in this document and functionality
offered by the individual endpoints should have a high degree of orthogonality
with no duplication of functionality between multiple endpoints.

When building functionality that requires new API endpoints that can't be be
made to meet the rules set ou ton this page, these endpoints should not be put
in under the 2.0 version but instead be treated as internal APIs. Internal
APIs live under the "internal" version string
(`https://api.bitbucket.org/internal/..`). The APIs are not documented and
can be modified or even dropped whenever desired.

## Documentation

Bitbucket uses [Swagger 2.0](http://swagger.io) for API documentation with
the actual documentation strings living inside the source code.

Every public API endpoint must be documented using the `@doc()` handler
decorator in Python. The swagger file is dynamically generated at runtime
and lives at
[https://api.bitbucket.org/swagger.json](https://api.bitbucket.org/swagger.json).
It contains all URLs patterns under the /2.0 context, even handlers that lack
a `@doc()` decorator.

It is the developer's responsibility to write the documentation and new public
APIs should not be merged until they have been documented.
 
## Temporarily Hiding New APIs from Documentation

Since Swagger automatically published any 2.0 API it finds, it is important to
be careful introducing new endpoints. If a newly introduced 2.0 API must be
kept secret during the rollout process, it can be explicitly hidden from
Swagger by decorating it with `@doc(hidden=True)`.

New, public APIs might be put behind a flag (in which case embedded links to
it in other objects need to be flagged as well). Note that putting it behind
a flag alone does not hide it from Swagger.

## Versioning and Deprecation

All APIs are versioned with the version number as the first path segments of
the URL. The current version is 2.0, but only a subset of the functionality
is currently available under 2.0 and we still rely heavily on older 1.0
endpoints.

No new functionality should be added to 1.0.

Links in API responses should always point to the latest version of a
resource. When an older endpoint gets replaced by a newer version, objects
that link to it should be updated.

When an endpoint is available under more than one version, all but the latest
version are considered to be deprecated. Deprecated APIs should be marked as
such in their public documentation.

Deprecated APIs should continue to be maintained and supported for a minimum
of 6 months before being removed. Where appropriate, a deprecated old endpoint
may then be replaced by a 301 Permanent Redirect pointing to the latest
version.

## Review Process

Major changes to the public APIs in Bitbucket should start with a design
document similar to this one. Implementation should not start until the
design document has been approved by developers in the team experienced in
API design (@csomme, @erik). Also, the implementation pull
request should get an approval from either Chris Somme or Erik van Zijst.

# Celery tasks

## Arguments to tasks must be basic Python types

Celery must serialize the arguments to a task when it is queued. Typically,
the serialization takes the form of pickling and that has been known to be
problematic. The easiest way to avoid such problems is to use basic Python
types. For example, use a repository's `id` rather than a `Repository`
instance.

## Collection argument should have small and predictable lengths

Remember that Celery needs to serialize the arguments for a task and place
them in the task queue (RabbitMQ). A collection with a very large length
has (at least in the past) lead to issues with serialization.

## Tasks shouldn't rely on any object's existence

Tasks need to handle entities being deleted between the task firing and the
task running. For example, when fetching a repository from the DB using its
`id`, tasks should catch `Repository.DoesNotExist` and return early in the
task, if they must.

While it may not be applicable to all tasks, always prefer to extract
information from the database records needed by the tasks instead of passing
model `id`s to the tasks (and expect them to query the DB). In addition to
being wasteful (in terms of DB queries), the object may no longer exist by
the time the task is executed.

For example, don't do this:

```python
from bitbucket.apps.repo2.tasks import remove_uploaded_images


def delete(repo):
    # Remove any files linked to this repo from S3
    queue_worker(remove_uploaded_images, args=[repo.id])
    repo.delete()


@task
def remove_uploaded_images(repo_id):
    try:
        repo = Repository.objects.get(id=repo_id)
    except Repository.DoesNotExist:
        # +1 for catching this exception, but -1 for failing to cleanup
        print 'Womp womp womp!'
        return
    bucket = s3_public_storage.bucket
    keys = bucket.get_all_keys(prefix=s3_bucket_prefix(repo))
    bucket.delete_keys(keys)
```

Do this instead:

```python
from bitbucket.apps.repo2.tasks import remove_uploaded_images


def delete(repo):
    # Remove any files linked to this repo from S3
    queue_worker(remove_uploaded_images, args=[s3_bucket_prefix(repo)])
    repo.delete()


@task
def remove_uploaded_images(bucket_prefix):
    bucket = s3_public_storage.bucket
    keys = bucket.get_all_keys(prefix=bucket_prefix)
    bucket.delete_keys(keys)
```

## Don't deploy a task and usage of that task together

If in a single deploy you were to add the new task and code that queues it,
the first batch of app servers that have been upgrade may queue the task only
to have a not-yet-upgraded app server pick up the task and blow up since it
doesn't have the task's code nor even know it exists.

The right procedure is:

1. Hide the callers of the task behind a switch.
2. Deploy the code for the new task with the switch turned off.
3. After the deploy is complete, turn the switch on.
4. In a follow up deploy, remove the switch.

The above procedure gets your feature deployed and working fast. But if you
aren't in a hurry, you could avoid using the switch altogether:

1. Deploy the code for the new task with no callers that queue the task.
2. In a follow-up deploy add the code that queues the task.

In both procedures, you have to do 2 deploys, but the first procedure gets
your feature operational faster at the cost of using switches and creating 2
migrations (one for adding the switch, the other to take it out).

## Don't remove a task and usage of that task together

This is the corollary of the previous guideline. If a task were to be deleted
along with all its usages, then it is possible during the deploy an app
server that still has the old code would queue the task and a Celery worker
from another app server that has been updated will pick up the task and fail.

The right procedure is:

1. Deploy the code that removes all callers of the task scheduled to be
removed.
2. In a follow-up deploy, remove the task itself.

# Django views

## Don't try to deploy a view and its usage in one go

During the deployment process, some of the servers could have the newer code
and others the older code. It is possible to get a page served by an updated
server, which will link to the newly created view in the front-end. It is also
possible for the request to be routed to an un-updated server, causing an
error.

The recommended procedure is:

1. Hide the usage of the view behind a switch.
2. Deploy the code for the new view with the switch turned off.
3. After the deploy is complete, turn the switch on.
4. In a follow up deploy, remove the switch.

The above procedure gets your feature deployed and working fast, while giving
you a safety valve to turn it off quickly if things go badly. Alternatively,
you could spread the creation of the view and its usage across separate
deployments, but the switch approach is recommended.

Note: This applies to new APIs too.

## Don't try to delete a view and its usages in one go

This is the corollary of the previous guideline. If a view and its usage were
deleted in the same deploy, it is possible for a page to be served by an
un-updated server, which could link to the now-deleted view. The request could
then be routed to an updated server, where the view doesn't exist, causing a
500 error.

Deploy the deletion of the usage of the view separately from the actual
deletion of the view itself.

Note: This applies to deleting APIs too.
